using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CheezyPeezy
{
    public class CompleteChecker : MonoBehaviour
    {
        [SerializeField] private GameFieldCreator creator;
        [SerializeField] private GameObject completePopup;
        [SerializeField] private MainController mainController;

        private Cell[,] cells;

        private void Awake()
        {
            creator.FieldCreated += (c) =>
            {
                cells = c;
            };
        }

        private void Update()
        {
            if (cells == null || !mainController.IsPlaying)
            {
                return;
            }

            foreach (Cell cell in cells)
            {
                if (cell.CellState != CellState.Occupied)
                {
                    return;
                }
            }

            completePopup.SetActive(true);
            mainController.IsPlaying = false;
        }
    }
}
