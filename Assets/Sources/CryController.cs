using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CheezyPeezy
{
    public class CryController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem l;
        [SerializeField] private ParticleSystem r;

        [SerializeField] private MainController mc;

        // Update is called once per frame
        void Update()
        {
            var e = l.emission;
            e.rateOverTime = mc.Left * 5;

            e = r.emission;
            e.rateOverTime = mc.Left * 5;
            //l.emission = e;
        }
    }
}
