using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace CheezyPeezy
{
    public class DraggableElement : MonoBehaviour
    {
        [SerializeField] private List<Vector2Int> occupateMap;
        [SerializeField] private AudioClip pickUpSound;
        [SerializeField] private AudioClip dropSound;
        [SerializeField] private AudioClip rotateSound;

        private AudioSource audioSource;

        private int orientation;
        private bool isFlipped;
        private Vector3 origin;
        private Tween tween;

        public int Orientation
        {
            get => orientation;
            set
            {
                orientation = value;
                transform.rotation = Quaternion.Euler(gameObject.transform.position.x, orientation, gameObject.transform.position.z);
                PlayRotateSound();
            }
        }

        private void PlayRotateSound()
        {
            audioSource.clip = rotateSound;
            audioSource.Play();
        }

        public bool Flipped
        {
            get => isFlipped;
            set
            {
                isFlipped = value;
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                PlayRotateSound();
            }
        }
        public List<Cell> OccupatedCells { get; private set; }

        private void Awake()
        {
            OccupatedCells = new List<Cell>(8);
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            origin = transform.position;
        }

        public List<Vector2Int> GetMap()
        {
            int value = orientation % 360;
            if (value < 0)
            {
                value += 360;
            }

            List<Vector2Int> map = new List<Vector2Int>();

            if (value == 270)
            {
                for (int i = 0; i < occupateMap.Count; i++)
                {
                    map.Add(new Vector2Int(occupateMap[i].y, -occupateMap[i].x));
                }
            }

            if (value == 90)
            {
                for (int i = 0; i < occupateMap.Count; i++)
                {
                    map.Add(new Vector2Int(-occupateMap[i].y, occupateMap[i].x));
                }
            }

            if (value == 180)
            {
                for (int i = 0; i < occupateMap.Count; i++)
                {
                    map.Add(new Vector2Int(-occupateMap[i].x, -occupateMap[i].y));
                }
            }

            if (value == 0)
            {
                for (int i = 0; i < occupateMap.Count; i++)
                {
                    map.Add(new Vector2Int(occupateMap[i].x, occupateMap[i].y));
                }
            }

            if (Flipped)
            {
                for (int i = 0; i < map.Count; i++)
                {
                    Vector2Int pos = map[i];
                    if (value % 180 == 0)
                    {
                        pos.x = -pos.x;
                    }

                    if (value % 180 == 90)
                    {
                        pos.y = -pos.y;
                    }
                    map[i] = pos;
                }
            }

            return map;
        }

        public void SetDraggable(bool value, bool returnBack)
        {
            if (value)
            {
                audioSource.clip = pickUpSound;
            }
            else
            {
                audioSource.clip = dropSound;

                if (returnBack)
                {
                    tween = transform.DOMove(origin, 0.5F);
                }
            }

            audioSource.Play();
        }

        private void OnDestroy()
        {
            tween.Kill();
        }

        public void ResettDraggable()
        {
            tween.Kill();
            tween = null;
            OccupatedCells = new List<Cell>(8);
            transform.position = origin;
        }
    }
}