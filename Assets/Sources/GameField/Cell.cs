using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CheezyPeezy
{
    public enum CellState { Free, Occupied, Selected }

    public class Cell : MonoBehaviour
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector2Int Position => new Vector2Int(X, Y);

        public CellState CellState { get; private set; } = CellState.Free;

        [SerializeField] private SpriteRenderer spriteRenderer;

        public void SetState(CellState state)
        {
            CellState = state;
            switch (state)
            {
                case CellState.Free:
                    spriteRenderer.color = Color.white;
                    break;

                case CellState.Occupied:
                    spriteRenderer.color = Color.red;
                    break;

                case CellState.Selected:
                    spriteRenderer.color = Color.green;
                    break;
            }
        }

        public void SetIndentFactor(float value)
        {
            spriteRenderer.transform.localScale = Vector3.one * value;
        }
    }
}
