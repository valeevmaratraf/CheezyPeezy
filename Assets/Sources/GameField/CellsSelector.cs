using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CheezyPeezy
{
    public class CellsSelector : MonoBehaviour
    {
        private Cell[,] cells;

        public void Initizialize(Cell[,] cells)
        {
            this.cells = cells;
        }

        public bool SetSelection(Cell anchorCell, List<Vector2Int> relPositions)
        {
            ResetSelection();

            for (int i = 0; i < relPositions.Count; i++)
            {
                Vector2Int pos = new Vector2Int(anchorCell.X + relPositions[i].x,
                    anchorCell.Y + relPositions[i].y);

                if (pos.y >= cells.GetLength(1) || pos.x >= cells.GetLength(0) ||
                    pos.y < 0 || pos.x < 0)
                {
                    return false;
                }

                if (cells[pos.x, pos.y].CellState == CellState.Occupied)
                {
                    return false;
                }
            }

            for (int i = 0; i < relPositions.Count; i++)
            {
                Vector2Int pos = new Vector2Int(anchorCell.X + relPositions[i].x,
                    anchorCell.Y + relPositions[i].y);

                cells[pos.x, pos.y].SetState(CellState.Selected);
            }

            return true;
        }

        public void ResetSelection()
        {
            foreach (Cell cell in cells)
            {
                if (cell.CellState == CellState.Selected)
                {
                    cell.SetState(CellState.Free);
                }
            }
        }

        public bool FixSelectablesFor(DraggableElement element)
        {
            bool isSelectedAny = false;
            foreach (var cell in cells)
            {
                if (cell.CellState == CellState.Selected)
                {
                    cell.SetState(CellState.Occupied);
                    element.OccupatedCells.Add(cell);
                    isSelectedAny = true;
                }
            }

            return isSelectedAny;
        }

        public void ReleaseSelectablesFor(DraggableElement element)
        {
            foreach (var cell in element.OccupatedCells)
            {
                cell.SetState(CellState.Free);
            }

            element.OccupatedCells.Clear();
        }
    }
}
