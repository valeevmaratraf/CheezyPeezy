using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.AI;

namespace CheezyPeezy
{
    public class GameFieldCreator : MonoBehaviour
    {
        [SerializeField] private int cellsWidthCount;
        [SerializeField] private int cellsHeightCount;
        [SerializeField] private GameObject cellPrefab;
        [SerializeField] private Transform from;
        [SerializeField] private Transform to;
        [SerializeField] private Transform root;
        [SerializeField] private float indentFactor;

        [SerializeField] private CellsSelector selector;

        public Action<Cell[,]> FieldCreated;

        private Cell[,] cells;

        public void CreateField(int x = 0, int y = 0)
        {
            if (x != 0)
            {
                cellsWidthCount = x;
            }

            if (y != 0)
            {
                cellsHeightCount = y;
            }

            if (cells != null)
            {
                foreach (Cell cell in cells)
                {
                    Destroy(cell.gameObject);
                }
            }

            cells = Generate();
            FieldCreated?.Invoke(cells);

            selector.Initizialize(cells);
        }

        private Cell[,] Generate()
        {
            float width = to.position.x - from.position.x;
            float height = to.position.z - from.position.z;

            var delta = new Vector2(width / cellsWidthCount, height / cellsHeightCount);

            Cell[,] cells = new Cell[cellsWidthCount, cellsHeightCount];

            for (int x = 0; x < cellsWidthCount; x++)
            {
                for (int y = 0; y < cellsHeightCount; y++)
                {
                    var cellGO = Instantiate(cellPrefab);
                    cellGO.name = $"{x}:{y}";

                    var cellComp = cellGO.GetComponent<Cell>();
                    cellComp.X = x;
                    cellComp.Y = y;

                    cells[x, y] = cellComp;

                    var pos = new Vector3(from.position.x + x * delta.x, from.position.y, from.position.z + y * delta.y);

                    cellGO.transform.position = pos;
                    cellGO.transform.localScale = new Vector3(delta.x, delta.y, 0.1F);
                    cellComp.SetIndentFactor(indentFactor);
                    cellGO.transform.position += Vector3.right * delta.x / 2;
                    cellGO.transform.position += Vector3.forward * delta.y / 2;
                    cellGO.transform.forward = Vector3.up;

                    cellGO.transform.SetParent(root.transform);
                }
            }

            return cells;
        }
    }
}
