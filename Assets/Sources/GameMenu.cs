using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CheezyPeezy
{
    public class GameMenu : MonoBehaviour
    {
        [SerializeField] private Button exitButton;
        [SerializeField] private Button continueButton;
        [SerializeField] private MainController mainController;


        private void OnEnable()
        {
            mainController.IsPlaying = false;
        }

        private void OnDisable()
        {
            mainController.IsPlaying = true;
        }

        private void Awake()
        {
            exitButton.onClick.AddListener(() =>
            {
                SceneManager.LoadSceneAsync(0);
            });

            continueButton.onClick.AddListener(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}
