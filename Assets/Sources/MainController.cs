using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CheezyPeezy
{
    public class MainController : MonoBehaviour
    {
        public static int CurrentLevel { get; set; } = 0;

        [SerializeField] private CellsSelector selector;
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private GameObject losePopup;
        [SerializeField] private GameFieldCreator creator;
        [SerializeField] private List<GameObject> levels;

        public DraggableElement CurrentDraggable { get; private set; }
        public float Timer { get; private set; }
        public float Left => (float)TimeSpan.FromSeconds(Timer).Ticks / levelTime.Ticks;

        private TimeSpan levelTime;
        private Ray screenRay;

        public bool IsPlaying { get; set; }

        private void Start()
        {
            StartGame(false);
        }

        public void StartGame(bool reset)
        {
            if (reset)
            {
                foreach (var draggables in levels)
                {
                    var drags = draggables.GetComponentsInChildren<DraggableElement>();
                    foreach (var drag in drags)
                    {
                        drag.ResettDraggable();
                    }
                }
            }

            creator.CreateField();
            SetTimer();
            IsPlaying = true;

            foreach (var draggables in levels)
            {
                draggables.gameObject.SetActive(false);
            }

            levels[CurrentLevel].gameObject.SetActive(true);
        }

        private void Update()
        {
            if (!IsPlaying)
            {
                return;
            }

            UpdateTimer();

            TrySetDragable();

            TryRotate();
            TryFlip();

            UpdateDragable();

            TryDrop();
        }

        private void UpdateTimer()
        {
            Timer += Time.deltaTime;
            var diff = levelTime - TimeSpan.FromSeconds(Timer);
            timerText.text = diff.ToString(@"mm\:ss");

            if (diff.Ticks < 0)
            {
                IsPlaying = false;
                losePopup.SetActive(true);
            }
        }

        private void SetTimer()
        {
            levelTime = TimeSpan.FromSeconds(60 * 3F);
            Timer = 0;
        }

        private void UpdateDragable()
        {
            if (CurrentDraggable == null)
            {
                return;
            }

            screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            var cellPos = SetSelection();

            Vector3 defaultPos = screenRay.GetPoint(0.4F);

            if (cellPos != Vector3.zero)
            {
                CurrentDraggable.transform.position = cellPos + Vector3.up * 0.005F;
            }
            else
            {
                CurrentDraggable.transform.position = defaultPos;
                selector.ResetSelection();
            }
        }

        private Vector3 SetSelection()
        {
            Vector3 hitPos = Vector3.zero;

            var result = Physics.RaycastAll(screenRay);
            foreach (var hit in result)
            {
                if (hit.collider.TryGetComponent(out Cell cell))
                {
                    var m = CurrentDraggable.GetMap();

                    bool canPlaced = selector.SetSelection(cell, m);
                    if (!canPlaced)
                    {
                        return hitPos;
                    }

                    hitPos = cell.transform.position;
                }
            }

            return hitPos;
        }

        private void TryDrop()
        {
            if (CurrentDraggable == null)
            {
                return;
            }

            if (Input.GetMouseButtonUp(0))
            {
                bool isOnGameField = selector.FixSelectablesFor(CurrentDraggable);
                CurrentDraggable.SetDraggable(false, !isOnGameField);
                CurrentDraggable = null;
            }
        }

        private void TrySetDragable()
        {
            if (CurrentDraggable != null)
            {
                return;
            }

            if (!Input.GetMouseButtonDown(0))
            {
                return;
            }

            screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            bool isMouseOnCollider = Physics.Raycast(screenRay, out var result);
            if (!isMouseOnCollider)
            {
                return;
            }

            var dragable = result.collider.GetComponentInParent<DraggableElement>();
            if (dragable == null)
            {
                return;
            }

            CurrentDraggable = dragable;
            CurrentDraggable.SetDraggable(true, false);
            if (CurrentDraggable.OccupatedCells.Count > 0)
            {
                selector.ReleaseSelectablesFor(CurrentDraggable);
            }
        }

        private void TryRotate()
        {
            if (CurrentDraggable == null)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                CurrentDraggable.Orientation += 90;
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                CurrentDraggable.Orientation -= 90;
            }
        }

        private void TryFlip()
        {
            if (CurrentDraggable == null)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                CurrentDraggable.Flipped = !CurrentDraggable.Flipped;
            }
        }
    }
}
