using Cysharp.Threading.Tasks;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CheezyPeezy
{
    public class IntroController : MonoBehaviour
    {
        [SerializeField] private List<Sprite> stages;
        [SerializeField] private Button skipButton;
        [SerializeField] private Image splash;
        [SerializeField] private Image frame;
        [SerializeField] private AudioSource ambient;
        [SerializeField] private AudioSource voice;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private AudioClip[] clips;

        [SerializeField, TextArea] private string[] texts;

        private bool skip;
        private bool initialized;

        private void Initialize()
        {
            gameObject.SetActive(true);

            if (initialized)
            {
                return;
            }

            skipButton.onClick.AddListener(() =>
            {
                skip = true;
            });

            initialized = true;
        }

        public async void StartIntro()
        {
            Initialize();

            splash.raycastTarget = true;
            splash.DOFade(0, 0);
            ambient.Play();
            await splash.DOFade(1, 0.5F).AsyncWaitForCompletion();
            GetComponent<CanvasGroup>().alpha = 1;

            for (int i = 0; i < stages.Count; i++)
            {
                Sprite stage = stages[i];
                frame.sprite = stage;
                text.text = texts[i];
                voice.DOFade(.4F, 0.5F);
                voice.clip = clips[i];
                voice.Play();
                await splash.DOFade(0, 0.5F).AsyncWaitForCompletion();
                splash.raycastTarget = false;

                await UniTask.WaitUntil(() =>
                {
                    return skip == true;
                });

                splash.raycastTarget = true;
                skip = false;

                await splash.DOFade(1, 0.5F).AsyncWaitForCompletion();
                voice.Stop();
                voice.DOFade(0, 0.5F);
            }

            await SceneManager.LoadSceneAsync(1);
        }
    }
}
