using CheezyPeezy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button startGameButton;
    [SerializeField] private Button exitGameButton;
    [SerializeField] private Button authorsGameButton;

    [SerializeField] private IntroController introController;

    private void Awake()
    {
        startGameButton.onClick.AddListener(StartGameButtonClickHandler);
        exitGameButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });
    }

    private void StartGameButtonClickHandler()
    {
        introController.StartIntro();
    }
}
