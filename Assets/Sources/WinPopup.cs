using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

namespace CheezyPeezy
{
    public class WinPopup : MonoBehaviour
    {
        [SerializeField] private Button continueButton;
        [SerializeField] private Button cancelButton;
        [SerializeField] private Button completeButton;

        [SerializeField] private TextMeshProUGUI txt;
        [SerializeField] private MainController mainController;

        [SerializeField] private AudioSource bgMusic;
        [SerializeField] private VideoPlayer vp;
        [SerializeField] private RawImage videoRender;
        [SerializeField] private GameObject content;
        [SerializeField] private VideoClip clip;

        private void Awake()
        {
            cancelButton.onClick.AddListener(() =>
            {
                SceneManager.LoadSceneAsync(0);
            });

            continueButton.onClick.AddListener(() =>
            {
                MainController.CurrentLevel++;
                mainController.StartGame(true);
                gameObject.SetActive(false);

                vp.gameObject.SetActive(false);
                videoRender.gameObject.SetActive(false);
                content.SetActive(false);
                bgMusic.Play();
            });

            completeButton.onClick.AddListener(() =>
            {
                SceneManager.LoadSceneAsync(0);
            });
        }

        private async void OnEnable()
        {
            content.gameObject.SetActive(false);
            vp.gameObject.SetActive(true);
            vp.url = Path.Combine(Application.streamingAssetsPath, "Win.mp4");

            videoRender.gameObject.SetActive(true);
            vp.frame = 0;

            bgMusic.Stop();
            vp.Play();

            await UniTask.WaitForSeconds(3.5F);

            content.SetActive(true);

            if (MainController.CurrentLevel >= 2)
            {
                txt.text = "�� ����, �������, ����� ��������� :) �� ������� ����������, ���� � ���������...";
                completeButton.gameObject.SetActive(true);
                cancelButton.gameObject.SetActive(false);
                continueButton.gameObject.SetActive(false);
                MainController.CurrentLevel = 0;
            }
        }
    }
}
